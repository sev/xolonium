# Release

version     := 4.2
weights     := Regular Bold
mainFormats := otf ttf
webFormats  := woff woff2


# Files

mainFonts    := $(foreach w,$(weights), $(foreach f,$(mainFormats), fonts/$(f)/Xolonium-$(w).$(f)))
webFonts     := $(foreach w,$(weights), $(foreach f,$(webFormats),  fonts/$(f)/Xolonium-$(w).$(f)))
xonoticFonts := xonotic/Xolonium-GPL-Regular.otf xonotic/Xolonium-GPL-Bold.otf


# Main rules

.PHONY: all
all: ${mainFonts}

.PHONY: web
web: ${webFonts}

.PHONY: xonotic
xonotic: ${xonoticFonts}

.PHONY: clean
clean:
	@ rm -fr tmp fonts
	@ rm -f ${xonoticFonts}


# Font files

fonts/otf/Xolonium-%.otf: \
	tmp/otf/Xolonium-%.otf \
	tmp/fea/Xolonium-%.fea
		@ mkdir -p $(@D)
		@ ./tools/clean-font.py $^ $@

fonts/ttf/Xolonium-%.ttf: \
	tmp/ttf/Xolonium-%.ttf \
	tmp/fea/Xolonium-%.fea
		@ mkdir -p $(@D)
		@ ./tools/clean-font.py $^ $@

fonts/woff/Xolonium-%.woff: \
	fonts/ttf/Xolonium-%.ttf
		@ mkdir -p $(@D)
		@ ./tools/make-woff.py $^ $@

fonts/woff2/Xolonium-%.woff2: \
	fonts/ttf/Xolonium-%.ttf
		@ mkdir -p $(@D)
		@ ./tools/make-woff.py $^ $@


# Temporary files

tmp/otf/Xolonium-%.otf: \
	src/sfd/Xolonium-%.sfd \
	src/sfd/Xolonium-Common.sfd \
	tmp/fea/Xolonium-%.fea
		@ mkdir -p $(@D)
		@ ./tools/make-font.py $^ NONE ${version} $@

tmp/ttf/Xolonium-%.ttf: \
	src/sfd/Xolonium-%.sfd \
	src/sfd/Xolonium-Common.sfd \
	tmp/fea/Xolonium-%.fea \
	tmp/hti/Xolonium-%.hti
		@ mkdir -p $(@D)
		@ ./tools/make-font.py $^ ${version} $@

.PRECIOUS: tmp/fea/Xolonium-%.fea
tmp/fea/Xolonium-%.fea: \
	src/fea/Xolonium-Common.fea \
	src/fea/Xolonium-Common-Class-Latin.fea \
	src/fea/Xolonium-Common-Class-Greek.fea \
	src/fea/Xolonium-Common-Class-Cyrillic.fea \
	src/fea/Xolonium-Common-Kern-All.fea \
	src/fea/Xolonium-Common-Kern-Latin.fea \
	src/fea/Xolonium-Common-Kern-Greek.fea \
	src/fea/Xolonium-Common-Kern-Cyrillic.fea \
	src/fea/Xolonium-Common-Kern.fea
		@ mkdir -p $(@D)
		@ sed -f tools/replace-names.sed $^ > $@

.PRECIOUS: tmp/hti/Xolonium-%.hti
tmp/hti/Xolonium-%.hti: \
	src/hti/Xolonium-Common-meta.hti \
	src/hti/Xolonium-%-cvt.hti \
	src/hti/Xolonium-Common-fpgm.hti \
	src/hti/Xolonium-%-prep.hti \
	src/hti/Xolonium-Common-glyphs.hti \
	src/hti/Xolonium-Common-composites.hti
		@ mkdir -p $(@D)
		@ cat $^ > $@


# Xonotic files

xonotic/Xolonium-GPL-%.otf: \
	tmp/otf/Xolonium-GPL-%.otf \
	tmp/fea/Xolonium-%.fea
		@ ./tools/clean-font.py $^ $@

tmp/otf/Xolonium-GPL-%.otf: \
	tmp/sfd/Xolonium-GPL-%.sfd \
	src/sfd/Xolonium-Common.sfd \
	tmp/fea/Xolonium-%.fea
		@ ./tools/make-font.py $^ NONE ${version} $@

tmp/sfd/Xolonium-GPL-%.sfd: \
	src/sfd/Xolonium-%.sfd
		@ mkdir -p $(@D)
		@ cp $^ $@
		@ sed -i 's|FontName: Xolonium|FontName: Xolonium-GPL|' $@
		@ sed -i 's|FullName: Xolonium|FullName: Xolonium GPL|' $@
		@ sed -i 's|FamilyName: Xolonium|FamilyName: Xolonium GPL|' $@
		@ sed -i 's|LangName:.*|LangName: 1033 "" "" "$*" "" "" "" "" "" "" "Severin Meyer" "" "https://gitlab.com/sev/xolonium" "" "This font is licensed under the GNU General Public License, either version 2 of the license, or any later version. This font is provided WITHOUT WARRANTY OF ANY KIND, either express or implied. See the GNU General Public License for more details.+AAoACgAA-As a special exception, if you create a document which uses this font, and embed this font or unaltered portions of this font into the document, this font does not by itself cause the resulting document to be covered by the GNU General Public License. This exception does not however invalidate any other reasons why the document might be covered by the GNU General Public License. If you modify this font, you may extend this exception to your version of the font, but you are not obligated to do so. If you do not wish to do so, delete this exception statement from your version." "https://www.gnu.org/licenses/gpl-2.0"|' $@
