#!/usr/bin/env python3
# Creates woff or woff2 webfont.

import sys
from fontTools import subset
from fontTools.ttLib import sfnt


# Read arguments
SOURCE = sys.argv[1] # *.ttf
TARGET = sys.argv[2] # *.woff|*.woff2


# Set zopfli iterations
sfnt.ZOPFLI_LEVELS[sfnt.ZLIB_COMPRESSION_LEVEL] = 500


# Save font
subset.main([
	SOURCE,
	"--glyphs=*",
	"--no-ignore-missing-unicodes",
	"--notdef-glyph",
	"--notdef-outline",
	"--recommended-glyphs",
	"--layout-features=*",
	"--hinting",
	"--no-desubroutinize",
	"--no-legacy-kern",
	"--name-IDs=*",
	"--no-name-legacy",
	"--no-glyph-names",
	"--no-legacy-cmap",
	"--no-symbol-cmap",
	"--no-recalc-bounds",
	"--no-recalc-timestamp",
	"--canonical-order",
	"--prune-unicode-ranges",
	"--no-recalc-average-width",
	"--with-zopfli",
	"--flavor=%s" % TARGET.split(".")[-1],
	"--output-file=%s" % TARGET
	])
