Xolonium
========

Xolonium is a futuristic typeface, with focus on legibility.
It provides support for Latin, Greek, and Cyrillic glyph sets,
and includes various pictograms and emoticons.

Xolonium originated as a custom font for the open-source game
[Xonotic](https://xonotic.org).


Build
-----

The fonts can be compiled with the provided Makefile,
which runs the scripts from the `tools` directory:

```
make all
```

Fonts can be compiled individually as well:

```
make fonts/ttf/Xolonium-Regular.ttf
```

The generated files can be deleted with:

```
make clean
```

FontForge might complain about the AGLFN or non-BMP kern pairs,
these are fixed during the build process.


Dependencies
------------

### All

- [FontForge](https://fontforge.github.io) 20200314
- [FontTools](https://github.com/fonttools/fonttools) 4.17.0

### TTF

- [Humble Type Instruction Compiler](https://gitlab.com/sev/htic) 3.7

### WOFF

- [Zopfli Python wrapper](https://pypi.python.org/pypi/zopfli)
- [Brotli Python wrapper](https://pypi.python.org/pypi/Brotli)


Xonotic
-------

The Makefile also builds a GPL version of the fonts, for
the game Xonotic. For this purpose, the metadata in the
name table is adjusted. Everything else remains identical.
